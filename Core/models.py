from __future__ import unicode_literals

from django.db import models


import django, os

from django.contrib.auth.models import User

from django.contrib.auth import logout

from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser)

def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)

# manage the output over here, how  ?
# how to manage the values over here ?


class MyUserManager(BaseUserManager):

    def create_user(self, email, password, date_of_birth,  gender, address, phone, first_name, last_name):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        print 'called2'
        user = self.model(
            email=self.normalize_email(email),
            date_of_birth=date_of_birth,
            gender=gender,
            address=address,
            phone=phone,
            first_name=first_name,
            last_name=last_name,
        )
        print 'called2'
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, date_of_birth,  gender, address, phone, first_name, last_name):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        print 'super'
        user = self.create_user(
            email,
            password=password,
            date_of_birth=date_of_birth,
            gender=gender,
            address=address,
            phone=phone,
            first_name=first_name,
            last_name=last_name
        )
        user.is_admin = True
        # user.is_staff = True
        user.save(using=self._db)
        return user


# register this class in the admin as well, how to manage this part over here ?
# manage the result over here

class InventoryUser(AbstractBaseUser):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=10)
    last_name = models.CharField(max_length=10)
    date_of_birth = models.DateField()
    profile_picture = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    choice_gender = (('m', 'm'), ('f', 'f'))
    gender = models.CharField(choices=choice_gender, max_length=2, default='m')
    address = models.TextField(blank=False)
    phone = models.CharField(blank=False, max_length=10)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    # is_staff = models.BooleanField(default=False)
    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['date_of_birth', 'gender', 'address', 'phone', 'first_name', 'last_name']


    def get_full_name(self):
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class Inventory(models.Model):
    # id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=20)
    description = models.TextField(blank=False, default='Empty')
    owner = models.ForeignKey(InventoryUser, related_name='owner')
    member = models.ManyToManyField(InventoryUser, related_name='member')


class Items(models.Model):
    # id = models.IntegerField(primary_key=True)
    description = models.TextField(blank=False)
    returnable = models.BooleanField(default=False)
    quantity_present = models.IntegerField(blank=False)
    inventory_associated = models.ForeignKey(Inventory, unique=True)


class UserProvisionItem(models.Model):
    user_id = models.ForeignKey(InventoryUser)
    item_id = models.ForeignKey(Items)
    quantity = models.IntegerField
    date_provision = models.DateTimeField
    date_return = models.DateTimeField


def logout_view(request):
    logout(request)
